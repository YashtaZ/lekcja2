#!/usr/bin/env python3
"""Kacper Wenta - Lesson2"""


class Flor:
    pass


class Cosmetic:
    pass


class Baking:
    pass


flora_of_poland = Flor()
flora_of_peru = Flor()
flora_of_portugal = Flor()

cream = Cosmetic()
deodorant = Cosmetic()
shampoo = Cosmetic()

roll = Baking()
kaiser_roll = Baking()
cake = Baking()

print(flora_of_peru, flora_of_poland, flora_of_portugal)
print(cream, deodorant, shampoo)
print(roll, kaiser_roll, cake)
